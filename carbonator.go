package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/daemon"
	"github.com/hashicorp/consul/api"
	"github.com/kelseyhightower/envconfig"
	"github.com/magiconair/properties"
)

var version string

type config struct {
	Service string `envconfig:"SERVICE" properties:"service"`
	Env     string `envconfig:"ENV" properties:"env"`
}

func main() {
	log.Printf("Starting Carbonator (git ref %s)\n", version)

	// Truncate vhost.properties, creating if it doesn't exist
	trunc, err := os.Create("/etc/carbonator/vhost.properties")
	if err != nil {
		log.Println(err.Error())
	}
	err = trunc.Close()
	if err != nil {
		log.Println(err.Error())
	}

	// Connect to Consul Agent
	client, err := api.NewClient(&api.Config{Address: "127.0.0.1:8500"})
	if err != nil {
		log.Fatalln(err.Error())
	}

	// Create dashed domain name
	_, dashDomain, err := getDomain()
	if err != nil {
		log.Fatalln(err.Error())
	}

	daemon.SdNotify(false, "READY=1")

	// Get the environment and properties file variables and parse them
	c, err := getConfig()
	if err != nil {
		log.Fatalln(err.Error())
	}

	log.Println("Service:", c.Service)
	log.Println("Env:", c.Env)

LOCK:

	for i := 1; i <= 100; i++ {
		dashVhost := generateVhost(c.Env, c.Service, i, dashDomain)
		// dotVhost := generateVhost(c.Env, c.Service, i, realDomain)
		path := fmt.Sprintf("carbonator/%s", dashVhost)

		opts := &api.LockOptions{
			Key:              path,
			LockTryOnce:      true,
			LockWaitTime:     100 * time.Millisecond,
			MonitorRetries:   10,
			MonitorRetryTime: 950 * time.Millisecond,
			SessionOpts: &api.SessionEntry{
				Checks:    []string{},
				Behavior:  "delete",
				TTL:       "10s",
				LockDelay: 100 * time.Millisecond,
			},
		}

		lock, err := client.LockOpts(opts)
		if err != nil {
			log.Fatalln(err.Error())
		}

		log.Printf("Trying to lock %s...\n", path)
		lockCh, err := lock.Lock(nil)

		if err != nil {
			log.Println("Locking error:", err.Error())
			log.Println("Retrying in 5s...")
			time.Sleep(5 * time.Second)
			goto LOCK
		}

		if lockCh == nil {
			continue
		}

		// From here onwards, we have a successful lock!
		log.Printf("Locked %s\n", path)

		// Lock JSON
		timestamp := time.Now().Format(time.RFC3339)
		keyMap := make(map[string]interface{})
		keyMap["timestamp"] = timestamp
		keyMap["hostname"], _ = os.Hostname()
		keyJSON, _ := json.MarshalIndent(keyMap, "", "\t")

		kv := client.KV()
		if _, err := kv.Put(&api.KVPair{
			Key:     path,
			Value:   keyJSON,
			Session: opts.Session,
			Flags:   api.LockFlagValue},
			&api.WriteOptions{}); err != nil {
			// TODO the chances for this error to occur here are almost zero.
			// It would occur if we "lost" Consul immediately after we create the lock.
			// For now, let's proceed here, but let's try to handle this error later,
			// ie. remove lock, goto LOCK, etc.
			log.Println(err.Error())
		}

		f, err := os.Create("/etc/carbonator/vhost.properties")
		if err != nil {
			// Failure to create this file is definitely a critical error
			log.Fatalln(err.Error())
		}

		f.WriteString(fmt.Sprintf("VHOST=%v", dashVhost))
		f.Close()

		sigCh, doneCh := make(chan os.Signal, 1), make(chan bool, 1)

		go func() {
			signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
			sig := <-sigCh
			log.Println("Received the", sig, "signal.")
			doneCh <- true
		}()

		select {
		case msg := <-lockCh:
			err := os.Truncate(f.Name(), 0)
			if err != nil {
				log.Println(err.Error())
			}
			log.Println("Error from lockCh:", msg)
			log.Println("Retrying in 5s...")
			time.Sleep(5 * time.Second)
			goto LOCK
		case <-doneCh:
			err := os.Truncate(f.Name(), 0)
			if err != nil {
				log.Println(err.Error())
			}
			log.Println("Unlocking and destroying lock...")
			if err := lock.Unlock(); err != nil {
				log.Fatalln(err.Error())
			} else {
				if err := lock.Destroy(); err != nil {
					log.Fatalln(err.Error())
				} else {
					log.Println("Lock unlocked and destroyed")
					os.Exit(0)
				}
			}
		}
	}
}

// getDomain returns the hostname and the dashed hostname as found in /etc/resolv.conf
func getDomain() (string, string, error) {
	f, err := os.Open("/etc/resolv.conf")
	if err != nil {
		return "", "", err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	var realDomain, dashDomain string

	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "search") {
			realDomain = strings.Split(scanner.Text(), " ")[1]
			dashDomain = strings.Replace(realDomain, ".", "-", -1)
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return "", "", err
	}

	return realDomain, dashDomain, nil
}

// normalizeEnv returns the env with a dash suffix if the env is not "default"
func normalizeEnv(env string) string {
	normEnv := fmt.Sprintf("%v-", env)
	if env == "default" {
		normEnv = ""
	}

	return normEnv
}

// generateVhost returns a dash vhost from the env, service, discovered index and dashed domain
func generateVhost(env, service string, i int, domain string) string {
	return fmt.Sprintf("%v%v%v-%v", normalizeEnv(env), service, i, domain)
}

// getConfig parses the environment and properties file variables,
// with former taking precendence, and outputs the deduced config struct.
func getConfig() (config, error) {
	var configEnv, configProp, configSum config
	props := properties.NewProperties()

	err := envconfig.Process("carbonator", &configEnv)
	if err != nil {
		log.Fatalln("Error parsing environment variables:", err.Error())
	}

	for configEnv.Service == "" && configProp.Service == "" {
		props, err = properties.LoadFile("/etc/carbonator/carbonator.properties", properties.UTF8)
		if err == nil {
			configProp.Service, _ = props.Get("SERVICE")
			configProp.Env, _ = props.Get("ENV")
		}
		if configProp.Service == "" {
			time.Sleep(1 * time.Second)
		}
	}

	configSum.Service = sumStrings(configEnv.Service, configProp.Service)
	configSum.Env = sumStrings(configEnv.Env, configProp.Env)

	if configSum.Env == "" {
		configSum.Env = "default"
	}

	if configSum.Service == "" {
		return config{}, errors.New("You must specify a service name")
	}

	return configSum, nil
}

// sumStrings gives precedence to the first string from the input, if it exists, and returns it.
// Returns the second string otherwise.
func sumStrings(primary, secondary string) string {
	if primary != "" {
		return primary
	}

	return secondary
}
